-----------------------------------------------------------------------------
-- |
-- Module      :  XMonad.Hooks.BrutalPrinter
-- License     :  BSD3-style
--
-- the heart of this module is brutalPrintStackSet function which
-- transforms stackset into a string that you can put into status bar
--
-----------------------------------------------------------------------------

module XMonad.Hooks.BrutalPrinter (
  brutalPrintStackSet, 
  defaultBrutalPrinter, 
  BP (..)
  ) where

import XMonad
import qualified XMonad.StackSet as SS
import XMonad.Util.WorkspaceCompare (getSortByTag)
import Data.Maybe (isJust)
import Data.Char (toLower, toUpper)
import Data.List (intersperse)
import Control.Monad
import Codec.Binary.UTF8.String (encodeString)

data BP = 
  BrutalPrinter { stackWithClass :: String -> String -> String
                , stackWithoutClass :: String -> String
                , currentStack :: String -> String
                , hiddenStack :: String -> String
                , hiddenNoWindowsStack :: String -> String
                , combiner :: [String] -> String
                }

-- | Taken from XMonad.Hooks.DynamicLog
-- |
xmobarColor :: String  -- ^ foreground color: a color name, or #rrggbb format
            -> String  -- ^ background color
            -> String  -- ^ output string
            -> String
xmobarColor fg bg output = concat ["<fc=", fg, if null bg then "" else "," ++ bg, ">", output, "</fc>"]

-- | Uniform string by making first latter upper case and the rest lower case
capitalize :: String -> String
capitalize (x : xs) = toUpper x : map toLower xs

emptyClassWorkaround :: String -> String
emptyClassWorkaround [] = "!@#$"        -- just to indicate that window has no printable class
emptyClassWorkaround windowClass = windowClass

spaceSeparated :: String -> String -> String
spaceSeparated tag windowClass = tag ++ " " ++ (emptyClassWorkaround $ capitalize windowClass)

-- | That's how I prefer to see my xmobar
defaultBrutalPrinter :: BP
defaultBrutalPrinter = 
  BrutalPrinter { stackWithoutClass = id
                , stackWithClass = spaceSeparated
                , currentStack = xmobarColor "orange2" ""
                , hiddenStack = id
                , hiddenNoWindowsStack = const ""
                , combiner = concat . intersperse " " . reverse . filter (not . null)
                }

brutalPrintStackSet :: BP -> WindowSet -> X String
brutalPrintStackSet bp ws = brutalStackSet bp ws >>= return . encodeString . combiner bp

brutalStackSet :: BP -> WindowSet -> X [String]
brutalStackSet bp winset = do
  workspaceSort <- getSortByTag
  let brutalFormat w = brutalWorkspace bp w >>= return . brutalPrinter winset w bp
  mapM brutalFormat . workspaceSort . SS.workspaces $ winset

brutalPrinter :: WindowSet -> SS.Workspace WorkspaceId l Window -> BP -> String -> String
brutalPrinter ws w
  | SS.tag w == SS.currentTag ws = currentStack
  | isJust (SS.stack w) = hiddenStack
  | otherwise = hiddenNoWindowsStack

brutalWorkspace :: BP -> SS.Workspace WorkspaceId l Window -> X String
brutalWorkspace bp ss = do
  windowClass <- brutalClass . SS.stack $ ss
  let tag = SS.tag ss
  return . brutalStack bp tag $ windowClass

getClass :: Window -> X String
getClass w = resClass `fmap` withDisplay (io . flip getClassHint w)

brutalStack :: BP -> String -> Maybe String -> String
brutalStack bp tag Nothing = stackWithoutClass bp tag
brutalStack bp tag (Just windowClass) = stackWithClass bp tag windowClass

-- | This window would be displayed instead of entire workspace.
-- | I prefer to see master's window class.
brutalWindow :: SS.Stack a -> a
brutalWindow = SS.focus

brutalClass :: Maybe (SS.Stack Window) -> X (Maybe String)
brutalClass = liftMaybe . fmap (getClass . brutalWindow)

liftMaybe :: Monad m => Maybe (m a) -> m (Maybe a)
liftMaybe (Just x) = liftM Just x
liftMaybe (Nothing) = return Nothing
