{-# OPTIONS_GHC -W -Wall #-}

-- --------------------------------------------------------------------------
-- |
-- Copyright   :  (c) Alexey Natalin 2013-2016
-- License     :  BSD3
--
-- Configuration module.
--
-----------------------------------------------------------------------------

import System.Exit
import Control.Monad
import Data.Maybe (isNothing, fromMaybe)
import XMonad
import XMonad.Actions.Search
import XMonad.Actions.SpawnOn (spawnHere, manageSpawn)
import XMonad.Actions.WithAll (sinkAll, killAll)
import XMonad.Actions.RotSlaves (rotAllUp, rotAllDown)
import XMonad.Actions.UpdatePointer (updatePointer)
import XMonad.Actions.Preview (preview)
import XMonad.Actions.SwapWorkspaces (swapWithCurrent)
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.BrutalPrinter (brutalPrintStackSet, defaultBrutalPrinter)
import XMonad.Hooks.SetWMName (setWMName)
import XMonad.Layout.NoBorders (smartBorders)
import XMonad.Layout.ThreeColumns
import XMonad.Layout.Grid
import XMonad.Prompt
import XMonad.Prompt.Shell
import XMonad.Util.EZConfig (mkKeymap)
import XMonad.Util.Run
import Data.List (intersperse, elem)
import Data.Map (union, fromList)
import qualified XMonad.StackSet as SS

defaults =
  defaultConfig { terminal           = "x-terminal-emulator"
                , focusFollowsMouse  = True
                , modMask            = mod4Mask
                , workspaces         = map show [1..9]
                , borderWidth        = 2
                , normalBorderColor  = "gray10"
                , focusedBorderColor = "SteelBlue1"
                , keys               = myKeys
                , mouseBindings      = myMouseBindings
                , layoutHook         = myLayout
                , manageHook         = myManageHook
                , logHook            = myLogHook
                }

myXPromptConfig =
  defaultXPConfig { position          = Top
                  , promptBorderWidth = 0
                  , historySize       = 1000
                  , historyFilter     = id
                  , bgColor           = "gray10"
                  , bgHLight          = "gray10"
                  , fgColor           = "gray"
                  , fgHLight          = "orange2"
                  , font              = "-*-terminus-medium-r-*-*-14-*-*-*-*-*-*-u"
                  , promptKeymap      = myXPKeymap
                  , alwaysHighlight   = False
                  }

mySearchConfig = 
  myXPromptConfig { alwaysHighlight = False }

myXPKeymap = let 
  isSep c = elem c " /:,._-\""
  in union (emacsLikeXPKeymap' isSep) $
     fromList [ ((controlMask, xK_c), quit) -- C-c
              , ((mod1Mask, xK_f), moveWord' isSep Next)
              , ((mod1Mask, xK_b), moveWord' isSep Prev)
              , ((mod1Mask, xK_d), killWord' isSep Next)
              , ((mod1Mask, xK_l), killWord' isSep Prev)
              ]

myKeys conf = mkKeymap conf $ concat [
  [                                     -- multimedia keys scrap
    ("<XF86AudioLowerVolume>",  unsafeSpawn "amixer -q set Master Playback Volume 10%- unmute"),
    ("<XF86AudioRaiseVolume>",  unsafeSpawn "amixer -q set Master Playback Volume 10%+ unmute"),
    ("<XF86AudioMute>",         unsafeSpawn "amixer -q set Master toggle")
  ]
  
  ++
  [                                     -- prompts
    ("M-u",           promptSearch mySearchConfig lucky),
    ("M-e",         promptShell "editor" (editor >=> spawnHere)),
    ("M-p",           promptShell "" spawnHere),
    ("M-S-<Return>",  promptShell "term" (terminalCommand (XMonad.terminal conf) >=> spawnHere))
  ]
  
  ++
  [                                     -- layouts manipulation
    ("M-<Tab>",       sendMessage NextLayout),
    ("M-<Space>",     setLayout $ XMonad.layoutHook conf),
    ("M-h",           sendMessage Shrink),
    ("M-l",           sendMessage Expand),
    ("M-x",           sendMessage ToggleStruts)
  ]
  
  ++
  [                                     -- primary window management
    ("M-k",         windows SS.focusDown),
    ("M-j",         windows SS.focusUp),
    ("M-<Escape>",  windows SS.focusMaster),
    ("M-<Return>",  windows SS.swapMaster),
    ("M-S-k",       windows SS.swapDown),
    ("M-S-j",       windows SS.swapUp),
    ("M-f",         rotAllUp),
    ("M-r",         rotAllDown)
  ]
  
  ++ 
  [                                     -- auxiliary window management
    ("M-S-w",   withUnfocused killWindow),
    ("M-c",     kill),
    ("M-S-c",   killAll),
    ("M-s",     withFocused $ windows . SS.sink),
    ("M-S-s",   sinkAll)
  ] 
  
  ++
  [                                     -- shortcuts
    ("M-q M-t", spawnHere $ XMonad.terminal conf),
    ("M-q M-e", editor "" >>= spawnHere),
    ("M-q M-f", spawnHere "x-www-browser"),
    ("M-q M-z", spawnHere "zathura"),
    ("M-q M-v", spawnHere "steam"),
    ("M-q <Home>", spawnHere "xmonad --restart"),
    ("M-q <End>", io (exitWith ExitSuccess))
  ]
  
  ++
  [("M-v M-" ++ w, windows $ shiftAndViewIfLast w) | w <- XMonad.workspaces conf]

  ++
  [("M-t M-" ++ w, preview w) | w <- XMonad.workspaces conf]
  
  ++
  [("M-o M-" ++ w, windows $ shiftAndView w) | w <- XMonad.workspaces conf]
  
  ++
  [("M-" ++ w, windows $ SS.view w) | w <- XMonad.workspaces conf]

  ++
  [("M-w M-" ++ w, windows $ swapWithCurrent w) | w <- XMonad.workspaces conf]
  ]

myMouseBindings conf = fromList $ [
  ((modm, button1), mouseMoveWindow),
  ((modm, button3), mouseResizeWindow)]
  where modm = XMonad.modMask conf

newtype WithPrefix = WithPrefix String

instance XPrompt WithPrefix where
  showXPrompt (WithPrefix prefix) = prefix ++ "> "

promptShell :: String -> (String -> X ()) -> X ()
promptShell label spawner = do
  commands <- io getCommands
  let predicate = (\ _ _ -> True)
  let shell = getShellCompl commands predicate
  mkXPrompt (WithPrefix label) myXPromptConfig shell spawner

promptSimple :: String -> (String -> X ()) -> X ()
promptSimple label = mkXPrompt (WithPrefix label) myXPromptConfig noCompletion
  where noCompletion = const (return [])

terminalCommand term cmd = do
  return $ term ++ (if null cmd then "" else " -e " ++ cmd)

editor file = do
  editor <- io getEditor
  return $ editor ++ (if null file then "" else " " ++ file)

-- Brings currently selected window on desired stack (marked as `tag') and
-- switches to that one.
shiftAndView tag = SS.view tag . SS.shift tag

-- Operates same as above, but switches only if the current stack becomes empty.
shiftAndViewIfLast tag ss = 
  if length (SS.index ss) == 1 then shiftAndView tag ss
  else SS.shift tag ss

withUnfocused :: (Window -> X ()) -> X ()
withUnfocused f = withWindowSet $ flip whenJust mapUnfocused . SS.stack . SS.workspace . SS.current
  where mapUnfocused s = mapM_ f $ SS.up s ++ SS.down s

-- Center mouse pointer on currently focused window.
myLogHook = updatePointer (0.5, 0.5) (0, 0)

-- manageSpawn for XMonad.Action.SpawnOn.spawnHere
-- manageDocks for XMonad.Hooks.ManageDocks.avoidStruts
myManageHook = manageSpawn <+> manageDocks

myLayout = smartBorders $ avoidStruts $ grid ||| twoColumns ||| threeColumns ||| Full
  where
    grid = GridRatio(4/3)
    threeColumns = ThreeColMid nmaster delta (1/2)
    twoColumns = Tall nmaster delta (3/4)
    nmaster = 1
    delta = 3/100

-- setWMName "LG3D" - is a dirty workaround for some kind of 
-- applications that don't recognize xmonad properly.
installWMName conf = do
  return $ conf {
    startupHook = startupHook conf >> setWMName "LG3D"
    }

main = xmonad =<< installWMName defaults
