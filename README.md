Overview
-------
This configuration is template configuration and xmobar tricks and custom shortcuts.

Mod key is set to Super key and Super key is swapped with Caps Lock under X.
```
setxkbmap -option caps:super
```

* `M-e` Launche editor prompt
* `M-S-e` Open custom file in editor
* `M-t` Launche terminal
* `M-r` Rotate all windows on on the current stack and keep focus in place
* `M-S-r` Rotate in reversed direction
* `M-<Escape>` Focus on the master window
* `M-x` Toggle status bar visibly
* `M-S-w` Close all windows except the focused one
* `M-u` Perform lucky search in google using firefox
* `M-p` Execute custom shell command
* `M-S-<Return>` Execute custom shell command in terminal
* `M-s` Sink focused window
* `M-S-s` Sink all windows on current workspace
* `M-<Tab>` Iterate throughout layouts
* `M-<Space>` Reset layout
* `M-q <Home>` Restart xmonad
* `M-q <End>` Close xmonad

There are also several shortcuts (prefixed with `M-q`) for launching some applications, for example:

* `M-q M-f` Launch firefox
* `M-q M-e` Launch $EDITOR (emacs, by deafult)

Full list of launching shortcuts see in the source files.

StackSet manupulation shortcuts:

* `M-o M-<tag>` Shift focused window and view.
* `M-w M-<tag>` Swap workspaces.
* `M-t M-<tag>` View destination workspace until <tag> key is released.
* `M-v M-<tag>` Simple shift command.

Requirements
------------
All you need is listed below:

*   xmonad-0.11 =<
*   xmonad-contrib-0.11 =<

Some features also require more packages (it is strongly optional):

*   urxvt-9.15
*   xmobar-0.11
*   alsamixer-1.0.23

Files
-----
*   xmonad.hs
*   other/.xmobarrc

How to use
----------
1. Install `xmonad` and `xmonad-contrib`.
2. Next, you should copy `xmonad.hs` into your xmonad directory (usually, `~/.xmonad/`).
3. Run `xmonad --recompile` and than `xmonad --restart`.
4. Install xmobar and put `other/.xmobarrc` into your home.

TODO
----

* Each window swap causes some wasteful redraws which causes flickering.
* More features from the dynamic logs.
* Better user input with prompt.