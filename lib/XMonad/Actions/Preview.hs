module XMonad.Actions.Preview
       ( preview
       ) where

import Control.Monad (unless)
import XMonad
import XMonad.Core
import XMonad.Operations
import qualified XMonad.StackSet as SS

-- xserver auto-repeat interval given in microseconds.
autoRepeatInterval = 30000

preview :: WorkspaceId -> X ()
preview tag = do
  oldTag <- fmap SS.currentTag $ gets windowset
  windows $ SS.view tag
  onKeyRelease $ \anyKey -> return True
  windows $ SS.view oldTag

-- Avoids auto-repeating key release events.
onKeyRelease :: (KeySym -> X Bool) -> X ()
onKeyRelease breaker = do
  XConf {theRoot = root, display = display} <- ask
  let nextKeyEvent = allocaXEvent $ \ptr -> do
        maskEvent display (keyPressMask .|. keyReleaseMask) ptr
        KeyEvent {ev_event_type = t, ev_keycode = c} <- getEvent ptr
        s <- keycodeToKeysym display c 0
        return (t, s)
  let peekEventType = allocaXEvent $ \ptr -> do
        maskEvent display (keyPressMask .|. keyReleaseMask) ptr
        noEvent <- waitForEvent display autoRepeatInterval
        if not noEvent then do
          KeyEvent {ev_event_type = t} <- getEvent ptr
          putBackEvent display ptr
          return (Just t)
          else return Nothing
  let processKeyEvent (t, s) = do
        pending <- io peekEventType
        stopped <- (
          if (pending /= Just keyPress) && (t == keyRelease)
          then breaker s
          else return False)
        unless stopped $ io nextKeyEvent >>= processKeyEvent
  io $ grabKeyboard display root False grabModeAsync grabModeAsync currentTime
  io nextKeyEvent >>= processKeyEvent
  io $ ungrabKeyboard display currentTime
